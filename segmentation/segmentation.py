import os
from imutils import contours
import cv2


def perform_segmentation(data, result_size=(50, 50), save_to_file=False):
    output = []
    if save_to_file:
        dir_exists = os.path.isdir('.tmp')
        if not dir_exists:
            os.makedirs('.tmp')
    for i, img in enumerate(data):
        tmp_output = []
        gray = img.astype('uint8')
        thresh = cv2.threshold(gray, 250, 255, cv2.THRESH_BINARY_INV)[1]
        cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        try:
            (cnts, _) = contours.sort_contours(cnts, method="left-to-right")
        except:
            continue

        for j, c in enumerate(cnts):
            x, y, w, h = cv2.boundingRect(c)
            x = x - 2
            y = y - 2
            w = w + 2
            h = h + 2
            cv2.drawContours(thresh, [c], 0, (255, 255, 255), -1)
            shape = thresh[y:y + h, x:x + w]
            shape = cv2.resize(shape, (result_size[0], result_size[1]), interpolation=cv2.INTER_AREA)

            tmp_output.append({"data": shape, "center": [x + w / 2, y + h / 2]})
            if save_to_file:
                cv2.imwrite(f'.tmp/img{i}_{j}.png', shape)
        output.append(tmp_output)
    return output
