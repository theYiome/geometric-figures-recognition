from PIL import Image
import numpy as np
import glob
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

def train_nn(viz=1):
    number_of_classes=3
    input_shape=(50,50,1)
    data_dir="../data/"
    triangle_file_list = glob.glob(data_dir+'triangle/*.png')
    triangle = np.array([np.array(Image.open(fname)) for fname in triangle_file_list])

    square_file_list = glob.glob(data_dir+'square/*.png')
    square = np.array([np.array(Image.open(fname)) for fname in square_file_list])

    circle_file_list = glob.glob(data_dir+'circle/*.png')
    circle = np.array([np.array(Image.open(fname)) for fname in circle_file_list])

    X=np.concatenate((triangle,square,circle))
    y=np.concatenate((np.full(len(triangle),0),np.full(len(square),1),np.full(len(circle),2)))
    X, y = shuffle(X, y)
    x_train,x_test,y_train,y_test=train_test_split(X,y,test_size=0.2)
    x_train = x_train.astype("float32") / 255
    x_test = x_test.astype("float32") / 255

    x_train = np.expand_dims(x_train, -1)
    x_test = np.expand_dims(x_test, -1)


    y_train = tf.keras.utils.to_categorical(y_train, number_of_classes)
    y_test = tf.keras.utils.to_categorical(y_test, number_of_classes)


    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.5),
            layers.Dense(number_of_classes, activation="softmax"),
        ]
    )

    batch_size = 128
    epochs = 15
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    history=model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)

 
    score = model.evaluate(x_test, y_test, verbose=0)
    print("Test loss:", score[0])
    print("Test accuracy:", score[1])
    
    model.save("../data/model")

    if viz:
        import pandas as pd
        import matplotlib.pyplot as plt

        pd.DataFrame(history.history).plot(figsize=(8,5))
        plt.grid(True)
        plt.gca().set_ylim(0, 1)
        plt.show()

if __name__ == "__main__":
    train_nn()