from tensorflow import keras
import numpy as np


class image_recognizer():
    model = keras.models.load_model('data/model')
    labels = ['triangle', 'square', 'circle']

    def test_image(self, img):
        img = img.astype("float32") / 255
        img = np.expand_dims(img, -1)
        return self.labels[np.argmax(self.model.predict(img))]


# example usage
if __name__ == "__main__":
    from PIL import Image
    import glob

    triangle_file_list = glob.glob('data/circle/img1_4.png')
    triangle = np.array([np.array(Image.open(fname)) for fname in triangle_file_list])

    nn = image_recognizer()
    print(f"nn predicted {nn.test_image(triangle)}, correct anwser is circle")
