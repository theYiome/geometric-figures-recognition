import random

def create_box(a, b, split_prob, horizontal_split_prob, depth=0):
    box = {
        "a": a,
        "b": b,
        "split_prob": split_prob,
        "horizontal_split_prob": horizontal_split_prob,
        "depth": depth
    }
    return box

def create_bounding_boxes(a: tuple, b: tuple, split_prob=1, split_change=0.12, horizontal_split_prob=0.4, horizontal_split_change=0.25):
    initial_box = create_box(a, b, split_prob, horizontal_split_prob)

    current_boxes = list()
    current_boxes.append(initial_box)

    modified = True
    while modified:
        modified = False
        tmp_boxes_list = list()
        for box in current_boxes:
            if box["split_prob"] > random.random():
                # split
                modified = True

                depth = box["depth"] + 1
                new_split_prob = split_prob - split_change * depth

                a0 = box["a"]
                a1 = box["b"]
                x0 = a0[0]
                y0 = a0[1]
                x1 = a1[0]
                y1 = a1[1]

                box0 = None
                box1 = None

                if box["horizontal_split_prob"] >= random.random():
                    # horizontal split
                    b0 = (x0, y0 + (y1 - y0)/2)
                    b1 = (x1, y0 + (y1 - y0)/2)

                    new_horizontal_split_prob = horizontal_split_prob - horizontal_split_change * depth
                    box0 = create_box(a0, b1, new_split_prob, new_horizontal_split_prob, depth)
                    box1 = create_box(b0, a1, new_split_prob, new_horizontal_split_prob, depth)
                else:
                    # vertical split
                    b0 = (x0 + (x1 - x0)/2, y1)
                    b1 = (x0 + (x1 - x0)/2, y0)

                    new_horizontal_split_prob = horizontal_split_prob + horizontal_split_change * depth
                    box0 = create_box(a0, b0, new_split_prob, new_horizontal_split_prob, depth)
                    box1 = create_box(b1, a1, new_split_prob, new_horizontal_split_prob, depth)
                
                tmp_boxes_list.append(box0)
                tmp_boxes_list.append(box1)
            else:
                # no split
                box["split_prob"] = 0
                tmp_boxes_list.append(box)
        # swap
        current_boxes = tmp_boxes_list.copy()

    return current_boxes