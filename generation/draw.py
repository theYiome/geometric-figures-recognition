from matplotlib.path import Path
import matplotlib.patches as patches
import numpy as np

def triangle(a, b, c, line_width=3, fill_color="None"):
    verts = [a, b, c, a]
    codes = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]
    path = Path(verts, codes)
    return patches.PathPatch(path, lw=line_width, facecolor=fill_color)

def quad(a, b, c, d, line_width=3, fill_color="None"):
    verts = [a, b, c, d, a]
    codes = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]
    path = Path(verts, codes)
    return patches.PathPatch(path, lw=line_width, facecolor=fill_color)

def circle(a, r, line_width=10, fill_color='k'):
    return patches.Circle(a, radius=r, color=fill_color, linewidth=line_width, fill=False)

def plt2arr(fig, draw=True):
    if draw:
        fig.canvas.draw()
    rgba_buf = fig.canvas.buffer_rgba()
    (w,h) = fig.canvas.get_width_height()
    rgba_arr = np.frombuffer(rgba_buf, dtype=np.uint8).reshape((h,w,4))
    return rgba_arr