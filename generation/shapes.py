import rectangle
import draw
import random
import math

def circle_description(center, r):
    return {
        "type": 1,
        "a": center,
        "r": r
    }

def triangle_description(a, b, c):
    return {
        "type": 2,
        "a": a,
        "b": b,
        "c": c
    }

def rectangle_description(a, b, c, d):
    return {
        "type": 3,
        "a": a,
        "b": b,
        "c": c,
        "d": d
    }

def generate_random_shape(a, b):
    shape = random.randint(1, 3)

    color = "None"
    if random.random() < 0.4:
        color = "#" + (random.choice("0123456789ABCDE") * 6)

    dx = b[0] - a[0]
    dy = b[1] - a[1]

    if shape == 1:
        # circle
        if color == "None":
            return None, None
        center = (a[0] + dx/2, a[1] + dy/2)
        distance = min(dx, dy) / 2
        r = distance * (0.2 + random.random() * 0.7)
        return draw.circle(center, r, line_width=random.randint(1, 2), fill_color=color), circle_description(center, r)
    elif shape == 2:
        # triangle
        a0 = (a[0] + dx * random.random() * 0.2, a[1] + dy * random.random() * 0.3)
        b0 = (b[0] - dx * random.random() * 0.4, a[1] + dy * random.random() * 0.2)
        c0 = (a[0] + dx * random.random(), b[1] - dy * random.random() * 0.3)
        return draw.triangle(a0, b0, c0, line_width=random.randint(1, 2), fill_color=color), triangle_description(a0, b0, c0)
    else:
        # rectangle
        center = (a[0] + dx/2, a[1] + dy/2)
        angle = math.pi * random.random()
        dm = min(dx, dy)
        w = dm * (0.2 + random.random() * 0.6)
        h = dm * (0.2 + random.random() * 0.6)
        a0, b0, c0, d0 = rectangle.rectangle(w, h, center, angle)
        return draw.quad(a0, b0, c0, d0, line_width=random.randint(1, 2), fill_color=color), rectangle_description(a0, b0, c0, d0)