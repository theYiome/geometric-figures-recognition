import numpy as np
import sys
import matplotlib.pyplot as plt
import json

import draw
import bounding
import shapes

data_size = 100
if len(sys.argv) > 1:
    data_size = int(sys.argv[1])

ratio = 1.5
data_array = None
data_descriptions = list()

for i in range(data_size):
    fig, ax = plt.subplots()
    boxes = bounding.create_bounding_boxes((0.0, 0.0), (ratio, 1.0), split_change=0.15)
    descriptions = list()
    for box in boxes:
        x0 = box["a"][0]
        y0 = box["a"][1]
        x1 = box["b"][0]
        y1 = box["b"][1]

        a = (x0, y0)
        c = (x1, y1)

        shape, description = shapes.generate_random_shape(a, c)
        if shape is not None:
            descriptions.append(description)
            ax.add_patch(shape)

    ax.set_xlim(0, ratio)
    ax.set_ylim(0, 1)

    ax.axis('off')
    data = draw.plt2arr(fig)[:, :, 0]
    plt.close()

    if data_array is None:
        w, h = data.shape
        data_array = np.zeros((data_size, w, h))

    data_array[i] = data
    data_descriptions.append(descriptions)

# save generated data and descriptions to files
with open('dataset_{}.npy'.format(data_size), 'wb') as file:
    np.save(file, data_array)

# print(json.dumps(data_descriptions, indent=4))
json_data = {
    "data": data_descriptions
}
with open('descriptions_{}.json'.format(data_size), 'w') as file2:
    json.dump(json_data, file2)