import math

# https://stackoverflow.com/questions/39879924/rotate-a-rectangle-consisting-of-4-tuples-to-left-or-right

def rotate(point, theta):
    # https://en.wikipedia.org/wiki/Rotation_matrix#In_two_dimensions
    cos_theta, sin_theta = math.cos(theta), math.sin(theta)

    return (
        point[0] * cos_theta - point[1] * sin_theta,
        point[0] * sin_theta + point[1] * cos_theta
    )


def translate(point, offset):
    return point[0] + offset[0], point[1] + offset[1]


def rectangle(w, h, offset=(0, 0), angle=math.pi/4):
    points = [
        (-w/2, -h/2),
        (w/2, -h/2),
        (w/2, h/2),
        (-w/2, h/2)
    ]

    return [translate(rotate(point, angle), offset) for point in points]


if __name__ == '__main__':
    l = rectangle(100, 100, (30, -70))
    print(l)