# Rozpoznawanie Figur Geometrycznych
## Do wykonania
- moduł do segmentyzacji
- sieć która potrafi zidentyfikować pojedynczą figurę

## Generowanie danych
```bash
cd generation
python3 generate_dataset.py <dataset size>
```
### Przykład generowania
```bash
cd generation
python3 generate_dataset.py 1000
```
Stworzy to nam dwa pliki:
```bash
dataset_1000.npy
descriptions_1000.json
```
### Przykład użycia
```python
import json
import numpy as np
import matplotlib.pyplot as plt

data = np.load("dataset_100.npy")

data_descriptions = None
with open("descriptions_100.json", 'r') as f:
    data_descriptions = json.load(f)

descriptions = data_descriptions["data"]

size = data.shape[0]
for i in range(3):
    current_data = data[i]
    plt.axis("off")
    plt.imshow(current_data, cmap='gray', vmin=0, vmax=255)
    plt.show()
    print(json.dumps(descriptions[i]))
```
Interaktywny przykład użycia znajduje się w **generation/data_reading.ipynb**

### Segmentacja
```python
data = np.load("generation/dataset_1.npy")
output = perform_segmentation(data, result_size=(50, 50), save_to_file=False)
```
Funkcja przyjmuje wygenerowany wcześniej dataset i dokonuje segmentacji na wszystkich znajdujących się w nich obrazach.

Format zwracanych danych:
```json
[
    [
        {
            "data": <numpy array>
            "center": <tuple<int>>  # przechowuje środkowy punkt obiektu na wejściowym obrazie (w pikselach)
        },
        ...
    ],
    [
        ...
    ],
    ...
]
```
### Rozpoznawanie kształtów
```python
nn=image_recognizer()
nazwa_ksztaltu=nn.test_image(img)
```
Funkcja klasyfikuje obrazek przypisując mu nazwę kształtu